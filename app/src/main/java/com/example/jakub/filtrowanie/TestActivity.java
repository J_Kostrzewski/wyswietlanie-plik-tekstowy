package com.example.jakub.filtrowanie;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import static com.example.jakub.filtrowanie.R.layout.support_simple_spinner_dropdown_item;

public class TestActivity extends AppCompatActivity {

	@Override
	protected void onCreate( Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_test );

		ArrayAdapter<String> adapterListy;
		ListView lv;
		ArrayList<String> showListWithoutFiltering;

		lv = (ListView) findViewById( R.id.listView );


		showListWithoutFiltering = getIntent().getStringArrayListExtra( "lista" );

		adapterListy = new ArrayAdapter<>( this, support_simple_spinner_dropdown_item, showListWithoutFiltering );
		lv.setAdapter( adapterListy );
	}
}
