package com.example.jakub.filtrowanie;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import static com.example.jakub.filtrowanie.R.layout.support_simple_spinner_dropdown_item;

public class MainActivity extends AppCompatActivity {

	Spinner              spinner;
	ListView             listView;
	ArrayAdapter         adapter1;
	ArrayAdapter<String> adapter2;
	public static String stringSpiner = "";

	@RequiresApi(api = Build.VERSION_CODES.N) @Override
	protected void onCreate( Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_main );

		spinner = (Spinner) findViewById( R.id.spinner );
		listView = (ListView) findViewById( R.id.listView );

		/* tekstowo
		final ObslugaPliku obslugaPliku = new ObslugaPliku( getApplicationContext() );
		obslugaPliku.readFile();
		obslugaPliku.copyList();
		obslugaPliku.filtrFile();*/


		final ObslugaPlikuBinarnie obslugaPlikuBinarnie = new ObslugaPlikuBinarnie( getApplicationContext() );

		obslugaPlikuBinarnie.readFile( getApplicationContext() );
		obslugaPlikuBinarnie.copyList();
		obslugaPlikuBinarnie.filtrFile();


		spinner.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
			@Override public void onItemSelected( final AdapterView<?> parent, final View view, final int position, final long id ) {
				/*
				tekstowo
				stringSpiner = (String) parent.getItemAtPosition( position );
				listView.setAdapter( adapter2 );
				obslugaPliku.showList();*/

				stringSpiner = (String) parent.getItemAtPosition( position );
				listView.setAdapter( adapter2 );
				obslugaPlikuBinarnie.showList();
			}

			@Override public void onNothingSelected( final AdapterView<?> parent ) {
			}
		} );

/*		tekstowo
		adapter1 = new ArrayAdapter<>( this, support_simple_spinner_dropdown_item, obslugaPliku.spinnerLista );
		spinner.setAdapter( adapter1 );
		adapter2 = new ArrayAdapter<>( this, support_simple_spinner_dropdown_item, obslugaPliku.wypisywanieListView );*/

		//binarnie
		adapter1 = new ArrayAdapter<>( this, support_simple_spinner_dropdown_item, obslugaPlikuBinarnie.spinnerLista );
		spinner.setAdapter( adapter1 );
		adapter2 = new ArrayAdapter<>( this, support_simple_spinner_dropdown_item, obslugaPlikuBinarnie.wypisywanieListView );


	}

	public boolean onCreateOptionsMenu( Menu menu ) {
		getMenuInflater().inflate( R.menu.activity_main, menu );
		return true;
	}

	public boolean onOptionsItemSelected( MenuItem item ) {
		switch ( item.getItemId() ) {
		case R.id.action_settings:
			final ObslugaPlikuBinarnie obslugaPlikuBinarnie = new ObslugaPlikuBinarnie( getApplicationContext() );
			obslugaPlikuBinarnie.readFile( getApplicationContext() );
			Intent intent = new Intent( this, TestActivity.class );
			intent.putStringArrayListExtra( "lista", obslugaPlikuBinarnie.nazwaFirmy );
			startActivity( intent );
			return true;
		default:
			return super.onOptionsItemSelected( item );
		}
	}

}
