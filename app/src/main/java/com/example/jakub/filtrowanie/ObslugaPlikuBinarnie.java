package com.example.jakub.filtrowanie;

import android.content.Context;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import static com.example.jakub.filtrowanie.MainActivity.stringSpiner;

class ObslugaPlikuBinarnie {

	final         ArrayList<String> wypisywanieListView      = new ArrayList<>();
	final         ArrayList<String> nazwaFirmy               = new ArrayList<>();
	final         ArrayList<String> spinnerLista             = new ArrayList<>();
	final         ArrayList<String> showListWithoutFiltering = new ArrayList<>();
	private final ArrayList<String> copy                     = new ArrayList<>();

	ObslugaPlikuBinarnie( final Context applicationContext ) {
	}


	void readFile( final Context applicationContext ) {
		String nrWersji = "";
		String nazwaKlienta = "";
		try {
			InputStream inputStream = applicationContext.getAssets().open( "wersje_projektow.bin" );
			DataInputStream dataInputStream = new DataInputStream( inputStream );
			for ( int i = 0; i < inputStream.available(); i++ ) {

				int index = ((dataInputStream.read() & 0xff) << 8) | (dataInputStream.read() & 0xff);
				int rozmiarWersji = ((dataInputStream.read() & 0xff) << 8) | (dataInputStream.read() & 0xff);
				//Log.d( " INDEX : ", index + " ROZMIAR: " + rozmiarWersji );

				for ( int j = 0; j < rozmiarWersji; j++ ) {
					nrWersji = nrWersji + String.valueOf( (char) dataInputStream.read() );
				}
				spinnerLista.add( nrWersji );

				int rozmiarProjektu = ((dataInputStream.read() & 0xff) << 8) | (dataInputStream.read() & 0xff);
				//Log.d( "Rozmiar Nazwy Klienta: ", String.valueOf( rozmiarProjektu ) );

				for ( int k = 0; k < rozmiarProjektu; k++ ) {
					nazwaKlienta = nazwaKlienta + String.valueOf( (char) dataInputStream.read() );
				}
				nazwaFirmy.add( nazwaKlienta );
				//Log.d( "WERSJA  : ", nrWersji + " Klient  : " + nazwaKlienta );

				nazwaKlienta = "";
				nrWersji = "";
			}
		} catch ( IOException e ) {
			e.printStackTrace();
		}

	}

	void copyList() {
		copy.addAll( spinnerLista );
	}

	void filtrFile() {
		for ( int i = 1; i < spinnerLista.size(); i++ ) {
			if ( spinnerLista.get( i ).equals( spinnerLista.get( i - 1 ) ) ) {
				spinnerLista.remove( i );
				i--;
			}
		}
	}

	ArrayList<String> showList() {
		wypisywanieListView.clear();
		if ( wypisywanieListView.size() == 0 ) {
			for ( int i = 0; i < copy.size(); i++ ) {
				if ( copy.get( i ).equals( stringSpiner ) ) {
					wypisywanieListView.add( nazwaFirmy.get( i ) );
				}
			}
		}
		return wypisywanieListView;
	}
}
