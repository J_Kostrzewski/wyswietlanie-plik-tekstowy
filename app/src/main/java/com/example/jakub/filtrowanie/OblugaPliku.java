package com.example.jakub.filtrowanie;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

import static com.example.jakub.filtrowanie.MainActivity.stringSpiner;

class ObslugaPliku {

	private Context myContext;

	final         ArrayList<String> wypisywanieListView      = new ArrayList<>();
	private final ArrayList<String> nazwaFirmy               = new ArrayList<>();
	final         ArrayList<String> spinnerLista             = new ArrayList<>();
	final         ArrayList<String> showListWithoutFiltering = new ArrayList<>();
	private final ArrayList<String> copy                     = new ArrayList<>();

	ObslugaPliku( Context myContext ) {
		this.myContext = myContext;
	}

	void readFile() {
		{
			try {
				final BufferedReader reader = new BufferedReader(
						new InputStreamReader( myContext.getAssets().open( "text" ), "UTF-8" ) );

				String mLine;
				while ( (mLine = reader.readLine()) != null ) {
					final StringTokenizer token = new StringTokenizer( mLine );

					spinnerLista.add( token.nextToken() );
					nazwaFirmy.add( token.nextToken() );
				}
			} catch ( IOException e ) {
				e.printStackTrace();
			}
		}
	}

	void copyList() {
		copy.addAll( spinnerLista );
	}

	void filtrFile() {
		for ( int i = 1; i < spinnerLista.size(); i++ ) {
			if ( spinnerLista.get( i ).equals( spinnerLista.get( i - 1 ) ) ) {
				spinnerLista.remove( i );
				i--;
			}
		}
	}

	ArrayList<String> showList() {
		wypisywanieListView.clear();
		if ( wypisywanieListView.size() == 0 ) {
			for ( int i = 0; i < copy.size(); i++ ) {
				if ( copy.get( i ).equals( stringSpiner ) ) {
					wypisywanieListView.add( nazwaFirmy.get( i ) );
				}
			}
		}
		return wypisywanieListView;
	}
}